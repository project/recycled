<?php
function phptemplate_preprocess_page(&$vars, $hook) {
  $vars['classes'] = _classes($vars['layout']);

  if (user_access('administer menu')) { // Edit Nav Menu
    $vars['edit_nav'] = l(theme_image(path_to_theme() .'/images/icons/edit.png', 'Edit Menu', 'Edit Menu'), 'admin/build/menu-customize/primary-links', array('query' => drupal_get_destination(), 'html' => TRUE));
  }
  // body classes
  $body_classes = array($vars['body_classes']);
  // node-edit class
  if (arg(0) == 'node' && (arg(2) == 'edit' || arg(1) == 'add')) {
    $body_classes[] = 'node-edit';
  }
  // Check what the user's browser is and add it as a body class    
  $user_agent = $_SERVER['HTTP_USER_AGENT'];
  if ($user_agent) {
    if (strpos($user_agent, 'MSIE')) {
      $body_classes[] = 'browser-ie';
    }
    else if (strpos($user_agent, 'MSIE 6.0')) {
      $body_classes[] = 'browser-ie6';
    }
    else if (strpos($user_agent, 'MSIE 7.0')) {
      $body_classes[] = 'browser-ie7';
    }
    else if (strpos($user_agent, 'MSIE 8.0')) {
      $body_classes[] = 'browser-ie8'; 
    }
    else if (strpos($user_agent, 'Firefox/2')) {
      $body_classes[] = 'browser-firefox2';
    }
    else if (strpos($user_agent, 'Firefox/3')) {
      $body_classes[] = 'browser-firefox3';
    }
    else if (strpos($user_agent, 'Safari')) {
      $body_classes[] = 'browser-safari';
    }
    else if (strpos($user_agent, 'Opera')) {
      $body_classes[] = 'browser-opera';
    }
  }
  $vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces

}

function phptemplate_preprocess_node(&$vars, $hook) {
  // Special classes for nodes
  $classes = array('node');
  if ($vars['sticky']) {
    $classes[] = 'sticky';
  }
  if (!$vars['status']) {
    $classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['teaser']) {
    $classes[] = 'node-teaser';
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $classes[] = 'node-type-' . $vars['type'];
  
  $vars['classes'] = implode(' ', $classes);
}

/* Ultimate 960 page helper function */
function _classes($layout) {
 switch ($layout) {
   case 'left':
     $classes['main'] = 'grid-9 push-3';
     $classes['left'] = 'grid-3 pull-9';
     $classes['content'] = 'grid-9 alpha omega';
     break;
   case 'right':
     $classes['main'] = 'grid-9';
     $classes['right'] = 'grid-3';
     break;
   case 'both':
     $classes['main'] = 'grid-6 push-3';
     $classes['left'] = 'grid-3 pull-6';
     $classes['right'] = 'grid-3';
     break;
   case 'none':
     $classes['main'] = 'grid-12';
     break;
  }
  return $classes;
}

function phptemplate_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];
  $classes = array('block');
  $classes[] = $vars['block']->module;
  $classes[] = $vars['block_zebra'];
  $classes[] = 'block-' . $vars['block_id'];
  if ($vars['block']->subject) {
     $classes[] = 'with-title';
  }
  if ($vars['block']->region == 'footer_inline') {
     $classes[] = 'grid-3';
     if ($vars['block_id'] == 1) {
       $classes[] = 'alpha';
     }
     elseif ($vars['block_id'] == 4) {
       $classes[] = 'omega';
     }
  }
  if ($vars['block']->region == 'content_top_inline' || $vars['block']->region == 'content_bottom_inline') {
     if (theme("blocks", "left") && theme("blocks", "right")) {
       $blockclasses = 'grid-3'; // both
     }
     elseif (theme("blocks", "left") || theme("blocks", "right")) {
       $blockclasses = 'grid-4'; // left or right
     }
     else {
       $blockclasses = 'grid-6'; // none
     }
     $classes[] = 'alpha';
     $classes[] = $blockclasses;
  }
  $vars['classes'] = implode(' ', $classes);

  if (user_access('administer blocks')) { // Block Edit Link
	  $vars['edit_block'] = l(theme_image(path_to_theme() .'/images/icons/edit.png', 'Edit Block', 'Edit Block'), 'admin/build/block/configure/'. $block->module .'/'. $block->delta, array('query' => drupal_get_destination(), 'html' => TRUE));
  }
}


/* Aurora Nav */
function _aurora_nav($tree) {
  $output = '';
  $items = array();
  // Pull out just the menu items we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if (!$data['link']['hidden']) {
      $items[] = $data;
    }
  }
  $num_items = count($items);
  foreach ($items as $i => $data) {
    $extra_class = NULL;
    if ($i == 0) {
      $extra_class = 'first';
    }
    if ($i == $num_items - 1) {
      $extra_class = 'last';
    }
    $link = theme('menu_item_link', $data['link']);
    if ($data['below']) {
      $output .= theme('menu_item', $link, $data['link']['has_children'], menu_tree_output($data['below']), $data['link']['in_active_trail'], $extra_class);
    }
    else {
      $output .= theme('menu_item', $link, $data['link']['has_children'], '', $data['link']['in_active_trail'], $extra_class);
    }
  }
  return $output ? '<ul class="menu sf-menu">'. $output .'</ul>' : '';
}